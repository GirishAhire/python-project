# single data type
# string
from ast import Delete


name=['Girish','Pranav','vivek','Suraj','Akash']
print(name)
# int 
numbers=[2,4,5,1,4]
print(name)
# float
decimal=[2.45,3.64,2.77]
print(decimal)
# boolean 
bool=[True,False]
print(bool)
# we use round brakets for pop
numbers.sort()
print(numbers)
# reverse
numbers.reverse()
print(numbers)
# clear
name.clear()
print(name)
info=["Girish",4,3.56,True]
# pop
info.pop()
print(info)
# sort
decimal.sort
print(decimal)
# insert
info.insert(1,"Ahire")
print(info)
# append(it adds only one value at the last of the list)
# info.append("hello")
# print(info)
# extend(we can add multiple values by using extend)
info.extend(["hey",'hello'])
print(info)
# remove(it is use to remove the element of the list,but it remove only single element)
info.remove("hey")
print(info)